import {Framework} from "floose";
import ComponentConfiguration = Framework.ComponentConfiguration;
import {AbstractMysqlModel} from "floose-driver-mysql";
import {User} from "floose-module-users";

export declare enum DriverType {
    MSSQL = "mssql",
    MYSQL = "mysql"
}

export declare enum ProviderCode {
    AMAZON_DEVICE_MESSAGING = "adm",
    APPLE_PUSH_NOTIFICATION = "apn",
    FIREBASE_CLOUD_MESSAGING = "fcm",
    WEB_NOTIFICATIONS = "web",
    WINDOWS_NOTIFICATION_SERVICE = "wns"
}

export declare enum MessageStatus {
    PENDING = "pending",
    SENDING = "sending",
    SENT = "sent",
    FAILED = "failed"
}

/**
 * CONFIGURATION
 */
export interface NotificationsModuleConfig extends ComponentConfiguration {
    driver: DriverType;
    connection: string;
    providers: {
        [ProviderCode.AMAZON_DEVICE_MESSAGING]?: {
            /**
             * Oauth client ID
             */
            clientId: string,
            /**
             * Oauth client secret
             */
            clientSecret: string
        };
        [ProviderCode.APPLE_PUSH_NOTIFICATION]?: {
            /**
             * The filename of the provider token key (as supplied by Apple) to load from disk, or a string containing the key data.
             */
            tokenKey: string;
            /**
             * The ID of the key issued by Apple
             */
            tokenKeyId: string;
            /**
             * ID of the team associated with the provider token key
             */
            tokenTeamId: string;
            /**
             * Specifies which environment to connect to: Production (if true) or Sandbox, the hostname will be set automatically.
             */
            production: boolean;
            /**
             * The topic of the remote notification, which is typically the bundle ID for your app
             */
            topic: string;
        };
        [ProviderCode.FIREBASE_CLOUD_MESSAGING]?: {
            /**
             * The FIREBASE_CLOUD_MESSAGING API KEY (https://firebase.google.com/docs/cloud-messaging)
             */
            id: string;
            /**
             * If true the phonegap settings is used
             */
            phonegap?: boolean;
        };
        [ProviderCode.WEB_NOTIFICATIONS]?: {
            // TODO: complete
        };
        [ProviderCode.WINDOWS_NOTIFICATION_SERVICE]?: {
            // TODO: complete
        };
    };
}

/**
 * MANAGERS
 */
export interface Payload {
    settings?: {
        [ProviderCode.AMAZON_DEVICE_MESSAGING]?: {
            /**
             * Used to define the notification lifespan
             * If a number was given the lifespan is considered as in seconds from now
             * If a date was given the lifespan is calculated from now as a date difference
             */
            expiry?: number | Date
            /**
             * Optional value. This is an arbitrary string used to indicate that multiple messages are logically the same and
             * that AMAZON_DEVICE_MESSAGING is allowed to drop previously enqueued messages in favor of this new one. Note that there are no
             * guarantees that the previously enqueued messages will not be delivered. Your consolidation key can be no greater
             * than 64 characters in length.
             */
            consolidationKey?: string;
            /**
             * Optional value. This is a base-64-encoded MD5 checksum of the data parameter. If you provide a value for the
             * md5 parameter, AMAZON_DEVICE_MESSAGING verifies its accuracy. If you do not provide a value, the server calculates the value on your
             * behalf. In either case, the server passes the md5 parameter's value through to the device and returns the
             * calculated value within the x-amzn-data-md5 response header.
             */
            md5?: string;
        };
        [ProviderCode.APPLE_PUSH_NOTIFICATION]?: {
            /**
             * A UUID to identify the notification with APNS. If an id is not supplied, APNS will generate one automatically.
             * If an error occurs the response will contain the id. This property populates the apns-id header.
             */
            id?: string;
            /**
             * Include this key when you want the system to display a standard alert or a banner.
             * The notification settings for your app on the user’s device determine whether an alert or banner is displayed.
             */
            alert?: {
                /**
                 * The title of the notification. Apple Watch displays this string in the short look notification interface.
                 * Specify a string that is quickly understood by the user.
                 */
                title?: string
                /**
                 * The key for a localized title string. Specify this key instead of the title key to retrieve the title
                 * from your app’s Localizable.strings files. The value must contain the name of a key in your strings file.
                 */
                titleLocKey?: string
                /**
                 * An array of strings containing replacement values for variables in your title string. Each %@ character in
                 * the string specified by the title-loc-key is replaced by a value from this array. The first item in the array
                 * replaces the first instance of the %@ character in the string, the second item replaces the second instance, and so on.
                 */
                titleLocArgs?: string[]
                /**
                 * Additional information that explains the purpose of the notification.
                 */
                subtitle?: string
                /**
                 * The key for a localized subtitle string. Use this key, instead of the subtitle key, to retrieve the subtitle
                 * from your app's Localizable.strings file. The value must contain the name of a key in your strings file.
                 */
                subtitleLocKey?: string
                /**
                 * An array of strings containing replacement values for variables in your title string. Each %@ character in
                 * the string specified by subtitle-loc-key is replaced by a value from this array. The first item in the array
                 * replaces the first instance of the %@ character in the string, the second item replaces the second instance, and so on.
                 */
                subtitleLocArgs?: string[]
                /**
                 * The content of the alert message.
                 */
                body?: string
                /**
                 * The key for a localized message string. Use this key, instead of the body key, to retrieve the message text
                 * from your app's Localizable.strings file. The value must contain the name of a key in your strings file.
                 */
                bodyLocKey?: string
                /**
                 * An array of strings containing replacement values for variables in your message text. Each %@ character in
                 * the string specified by loc-key is replaced by a value from this array. The first item in the array replaces
                 * the first instance of the %@ character in the string, the second item replaces the second instance, and so on.
                 */
                bodyLocArgs?: string[]
                /**
                 * The name of the launch image file to display. If the user chooses to launch your app, the contents of the
                 * specified image or storyboard file are displayed instead of your app's normal launch image.
                 */
                launchImage?: string
            }
            /**
             * The number to display in a badge on your app’s icon. Specify 0 to remove the current badge, if any.
             */
            badge?: number
            /**
             * The name of a sound file in your app’s main bundle or in the Library/Sounds folder of your app’s container
             * directory. Specify the string "default" to play the system sound. Use this key for regular notifications.
             * For critical alerts, use the sound dictionary instead.
             */
            sound?: string | {
                /**
                 * The critical alert flag. Set to 1 to enable the critical alert.
                 */
                critical?: boolean
                /**
                 * The name of a sound file in your app’s main bundle or in the Library/Sounds folder of your app’s container
                 * directory. Specify the string "default" to play the system sound.
                 * For information about how to prepare sounds, see UNNotificationSound.
                 */
                name?: string
                /**
                 * The volume for the critical alert’s sound. Set this to a value between 0.0 (silent) and 1.0 (full volume).
                 */
                volume?: number
            }
            /**
             * An app-specific identifier for grouping related notifications.
             * This value corresponds to the threadIdentifier property in the UNNotificationContent object.
             */
            threadId?: string
            /**
             * The notification’s type. This string must correspond to the identifier of one of the UNNotificationCategory objects you register at launch time.
             * See Declaring Your Actionable Notification Types (https://developer.apple.com/documentation/usernotifications/declaring_your_actionable_notification_types).
             */
            category?: string
            /**
             * The background notification flag. To perform a silent background update, specify the value 1 and don't include
             * the alert, badge, or sound keys in your payload
             */
            contentAvailable?: boolean
            /**
             * The notification service app extension flag. If the value is 1, the system passes the notification to your
             * notification service app extension before delivery. Use your extension to modify the notification’s content.
             * See Modifying Content in Newly Delivered Notifications (https://developer.apple.com/documentation/usernotifications/modifying_content_in_newly_delivered_notifications)
             */
            mutableContent?: boolean
            /**
             * The priority of the notification (default is "high"):
             * - Specify "high" to send the notification immediately. A value of "high" is appropriate for notifications that
             *   trigger an alert, play a sound, or badge the app’s icon. It's an error to specify this priority for a
             *   notification whose payload contains the content-available key.
             * - Specify "low" to send the notification based on power considerations on the user’s device. Use this priority
             *   for notifications whose payload includes the content-available key. Notifications with this priority might be
             *   grouped and delivered in bursts to the user’s device. They may also be throttled, and in some cases not delivered.
             */
            priority?: "low" | "high";
            /**
             * The date at which the notification is no longer valid. If the value is nonzero, APNs stores the notification and
             * tries to deliver it at least once, repeating the attempt as needed until the specified date. If the value is 0,
             * APNs attempts to deliver the notification only once and does not store the notification.
             */
            expiration?: number | Date
            /**
             * An identifier used to coalesce multiple notifications into a single notification for the user. Typically, each
             * notification request causes a new notification to be displayed on the user’s device. When sending the same
             * notification more than once, use the same value in this header to coalesce the requests.
             * The value of this key must not exceed 64 bytes.
             */
            collapseId?: string;
        };
        [ProviderCode.FIREBASE_CLOUD_MESSAGING]?: {
            /**
             * Used to define the priority to send notification
             * As default HIGH is used
             */
            priority?: "low" | "high";
            /**
             * Used to define the notification lifespan
             * If a number was given the lifespan is considered as in seconds from now
             * If a date was given the lifespan is calculated from now as a date difference
             */
            expiry?: number | Date;
            /**
             * Required. Indicates notification title. This field is not visible on iOS phones and tablets.
             */
            title: string;
            /**
             * Indicates notification body text.
             */
            body?: string;
            /**
             * Required. Indicates notification icon. Sets value to myicon for drawable resource myicon.
             */
            icon: string;
            /**
             * Indicates a sound to play when the device receives the notification. Supports default, or the filename
             * of a sound resource bundled in the app. Android sound files must reside in /res/raw/.
             */
            sound?: string;
            /**
             * Indicates whether each notification message results in a new entry on the notification center on Android.
             * If not set, each request creates a new notification. If set, and a notification with the same tag is already
             * being shown, the new notification replaces the existing one in notification center.
             */
            tag?: string;
            /**
             * Indicates color of the icon, expressed in #rrggbb format
             */
            color?: string;
            /**
             * The action associated with a user click on the notification.
             * If this is set, an activity with a matching intent filter is launched when user clicks the notification.
             */
            clickAction?: string;
            /**
             * Indicates the key to the body string for localization.
             * Use the key in the app's string resources when populating this value.
             */
            bodyLocKey?: string;
            /**
             * Indicates the string value to replace format specifiers in body string for localization.
             * These are the format arguments for the string resource.
             */
            bodyLocArgs?: string;
            /**
             * Indicates the key to the title string for localization.
             * Use the key in the app's string resources when populating this value.
             */
            titleLocKey?: string;
            /**
             * Indicates the string value to replace format specifiers in title string for localization.
             * These are the format arguments for the string resource.
             */
            titleLocArgs?: string;
            /**
             * This parameter identifies a group of messages (e.g., with collapse_key: "Updates Available")
             * that can be collapsed, so that only the last message gets sent when delivery can be resumed.
             * This is intended to avoid sending too many of the same messages when the device comes back
             * online or becomes active.
             * Note that there is no guarantee of the order in which messages get sent.
             * Note: A maximum of 4 different collapse keys is allowed at any given time. This means a GCM connection
             * server can simultaneously store 4 different send-to-sync messages per client app. If you exceed this number,
             * there is no guarantee which 4 collapse keys the GCM connection server will keep.
             */
            collapseKey?: string;
            /**
             * This parameter specifies the package name of the application where the registration tokens must match
             * in order to receive the message.
             */
            restrictedPackageName?: string;
            /**
             * This parameter, when set to true, allows developers to test a request without actually sending a message.
             */
            dryRun?: boolean;
            /**
             * How many times to retry send message. Default 0.
             */
            retries?: number;
        };
        [ProviderCode.WEB_NOTIFICATIONS]?: {
            // TODO: complete
        };
        [ProviderCode.WINDOWS_NOTIFICATION_SERVICE]?: {
            // TODO: complete
        };
    };
    data?: {
        [key: string]: any;
    };
}
export interface Response {
    sent: number;
    failed: number;
}
export declare class NotificationManager {
    protected constructor();
    static getInstance(): NotificationManager;

    // message management
    sendMessageToUser(payload: Payload, user: User | User[]): Promise<Response>;
    sendMessageToGroup(payload: Payload, group: Group | Group[]): Promise<Response>;
    sendMessageToDevice(payload: Payload, device: Device | Device[]): Promise<Response>;
    getMessageById(id: string | string[]): Promise<Message | Message[]>;
    getMessageByDevice(device: Device | Device[]): Promise<Message[]>;
    getMessageByUser(user: User): Promise<Message[]>;

    // device management
    getDevice(id: string | string[]): Promise<Device> | Promise<Device[]>;
    getDeviceByToken(provider: ProviderCode, token: string): Promise<Device>;
    registerDevice(user: User, provider: ProviderCode, token: string, name?: string): Promise<Device>;
    unregisterDevice(device: Device): Promise<void>;

    // group management
    getGroup(id: string | string[]): Promise<Group> | Promise<Group[]>;
    createGroup(name?: string): Promise<Group>;
    deleteGroup(group: Group): Promise<void>;
    addDeviceToGroup(group: Group, device: Device): Promise<Group>;
    removeDeviceFromGroup(group: Group, device?: Device|Device[]): Promise<Group>;
}

/**
 * MODELS
 */
export class MessageSchema {
    static readonly TABLE_NAME: string;
}
export declare class Message extends AbstractMysqlModel {
    id: string;
    device: string;
    message: string;
    status: MessageStatus;
    sentAt: Date;
    createdAt: Date;
    updatedAt: Date;
}
export class DeviceSchema {
    static readonly TABLE_NAME: string;
}
export declare class Device extends AbstractMysqlModel {
    id: string;
    user: string;
    provider: ProviderCode;
    enabled: boolean;
    token: string;
    name: string | undefined;
    createdAt: Date;
    updatedAt: Date;
}
export class GroupSchema {
    static readonly TABLE_NAME: string;
}
export declare class Group extends AbstractMysqlModel {
    id: string;
    enabled: boolean;
    name: string | undefined;
    createdAt: Date;
    updatedAt: Date;
}
